const moment = require('moment');

const response = require('../common/response');

let getNowYearToSec = () => {
    try {
        let nowDateYearToSec = moment().format('YYYY-MM-DD HH:mm:ss');

        return response.setResultToController(response.RESULT.SUCCESS.code, nowDateYearToSec);
    } catch (error) {
        return response.setResultToController(response.RESULT.SYSTEM_ERR.code, error);
    }
}

let getNowYearToDate = () => {
    try {
        let nowDateYearToDate = moment().format('YYYY-MM-DD');
    
        return response.setResultToController(response.RESULT.SUCCESS.code, nowDateYearToDate);
    } catch (error) {
        return response.setResultToController(response.RESULT.SYSTEM_ERR.code, error);
    }
}

let convertDateToPath = (date) => {
    try {
        let path = moment(date).format('YYYY/MM/DD');
    
        return response.setResultToController(response.RESULT.SUCCESS.code, path);
    } catch (error) {
        return response.setResultToController(response.RESULT.SYSTEM_ERR.code, error);
    }
}

module.exports = {
    getNowYearToSec,
    getNowYearToDate,
    convertDateToPath
}