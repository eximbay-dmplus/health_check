const logger = require('../common/logger').logger;

const RESULT = {
    SUCCESS: {code:200, string:'SUCCESS'},
    BAD_REQUEST: {code:400, string:'BAD REQUEST'},
    UNAUTHORIZED: {code:401, string:'UNAUTHORIZED'},
    PAYMENT_REQUIRED: {code:402, string:'PAYMENT REQUIRED'},
    FORBIDDEN: {code:403, string:'FORBIDDEN'},
    NOT_FOUND: {code:404, string:'NOT FOUND'},
    NOT_ACCEPTABLE: {code:406, string:'NOT ACCEPTABLE'},
    CONFLICT: {code:409, string:'CONFLICT'},
    SYSTEM_ERR: {code:500, string:'SYSTEM ERROR'}
};

const RESULT_ENUM = {
    200: {code:200, string:'SUCCESS'},           //SUCCESS
    400: {code:400, string:'BAD REQUEST'},       //BAD_REQUEST
    401: {code:401, string:'UNAUTHORIZED'},      //UNAUTHORIZED
    402: {code:402, string:'PAYMENT REQUIRED'},  //PAYMENT_REQUIRED
    403: {code:403, string:'FORBIDDEN'},         //FORBIDDEN
    404: {code:404, string:'NOT FOUND'},         //NOT_FOUND
    406: {code:406, string:'NOT ACCEPTABLE'},    //NOT_ACCEPTABLE
    409: {code:409, string:'CONFLICT'},          //CONFLICT
    500: {code:500, string:'SYSTEM ERROR'}       //SYSTEM_ERR
};

const METHOD_TYPE = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE'
};

const RESULT_VALID_NID = {
    SUCCESS: 0,
    FAIL_DECODED: -1,
    FAIL_NOT_EQUAL: -2
};

sendResultToClient = function(code, data, res){
    try {
        let body = {
            result: {
                resultCode: RESULT_ENUM[code]['code'],
                resultMsg: RESULT_ENUM[code]['string']
            },
            data: data
        };
        //logger.info(JSON.stringify(data));
        res.set('code', code);
        return res.send(body);
    }
    catch (err) {
        console.error(err);
        return new Error("Can't make response data...");
    } 
}

setResultToController = function(code, data){
    let body = {};
    body = {
        cd: RESULT_ENUM[code]['code'],
        data: data
    }
    return body;
}

checkErrorCode = function(code) {
    let errorCode = RESULT.SUCCESS.code;
    switch(code) {
        case RESULT.SUCCESS.code:
        case RESULT.BAD_REQUEST.code:
        case RESULT.UNAUTHORIZED.code:
        case RESULT.PAYMENT_REQUIRED.code:
        case RESULT.FORBIDDEN.code:
        case RESULT.NOT_FOUND.code:
        case RESULT.NOT_ACCEPTABLE.code:
        case RESULT.CONFLICT.code:
        case RESULT.SYSTEM_ERR.code:
            errorCode = code;
            break;
        case 1062:
            errorCode = RESULT.CONFLICT.code;
            break;
        case 1064:
        default:
            errorCode = RESULT.SYSTEM_ERR.code;
            break;
    }
    return errorCode;
};

getErrorString = function(code) {
    let errorString = RESULT.SUCCESS.string;
    switch(code) {
        case RESULT.SUCCESS.code:
            errorString = RESULT.SUCCESS.string;
            break;
        case RESULT.BAD_REQUEST.code:
            errorString = RESULT.BAD_REQUEST.string;
            break;
        case RESULT.UNAUTHORIZED.code:
            errorString = RESULT.UNAUTHORIZED.string;
            break;
        case RESULT.PAYMENT_REQUIRED.code:
            errorString = RESULT.PAYMENT_REQUIRED.string;
            break;
        case RESULT.FORBIDDEN.code:
            errorString = RESULT.FORBIDDEN.string;
            break;
        case RESULT.NOT_FOUND.code:
            errorString = RESULT.NOT_FOUND.string;
            break;
        case RESULT.NOT_ACCEPTABLE.code:
            errorString = RESULT.NOT_ACCEPTABLE.string;
            break;
        case RESULT.CONFLICT.code:
            errorString = RESULT.CONFLICT.string;
            break;
        case RESULT.SYSTEM_ERR.code:
            errorString = RESULT.SYSTEM_ERR.string;
            break;
    }
    return errorString;
};

checkValidationToken = function(type, req, res) {
    try {
        if (!req.decoded) {
            sendResponseFailDataNid(req, res);
            return RESULT_VALID_NID.FAIL_DECODED;
        } else {
            if(type === this.METHOD_TYPE.GET) {
                if (req.query.nid !== req.decoded._nid) {
                    sendResponseFailDataNotNId(req, res);
                    return RESULT_VALID_NID.FAIL_NOT_EQUAL;
                }
            }
            else {
                if (req.body.nid !== req.decoded._nid) {
                    sendResponseFailDataNotNId(req, res);
                    return RESULT_VALID_NID.FAIL_NOT_EQUAL;
                }
            }
            return true;
        }
    }
    catch(err) {
        console.error(err);
        return new Error("Can't make response data...");
    }
};

sendResponseResult = function(result, data, res) {
    try {
        let body = {
            result: result,
            data: data
        };
        return res.send(body);
    }
    catch (err) {
        console.error(err);
        return new Error("Can't make response data...");
    }
};

sendResponseFailDataNid = function(req, res) {
    try {
        let result = {};
        if( !req.decoded_err)
        {
            result = {
                resultCode : this.RESULT.BAD_REQUEST.code,
                resultMsg : 'can not found access token.'
            };
        }
        else
        {
            result = {
                resultCode : this.RESULT.BAD_REQUEST.code,
                resultMsg : 'access token is invalid.'
            };
        }
        let body = {
            result: result
        };
        return res.send(body);
    }
    catch (err) {
        console.error(err);
        return new Error("Can't make response data...");
    }
};

sendResponseFailDataNotNId = function(req, res) {
    try {
        let result;
        result = {
            resultCode : this.RESULT.BAD_REQUEST.code,
            resultMsg : 'NID is not equal.'
        };
        let body = {
            result: result
        };
        return res.send(body);
    }
    catch (err) {
        console.error(err);
        return new Error("Can't make response data...");
    }
};

sendResponseFailDataParameter = function(res) {
    try {
        let result;
        result = {
            resultCode : this.RESULT.BAD_REQUEST.code,
            resultMsg : this.RESULT.BAD_REQUEST.string
        };
        let body = {
            result: result
        };
        return res.send(body);
    }
    catch (err) {
        console.error(err);
        return new Error("Can't make response data...");
    }
};

sendResponseFailData = function(err, res) {
    try {
        let result;
        result = {
            resultCode : this.RESULT.SYSTEM_ERR.code,
            resultMsg : 'response data is error.'
        };
        let body = {
            result: result,
            data: {}
        };
        return res.send(body);
    }
    catch(err)
    {
        console.error(err);
        return new Error("Can't make response data...");
    }
};

sendResponseFailSystem = function(statusCode, res) {
    try {
        let result;
        if( statusCode !== 500 )
        {
            result = {
                resultCode : this.RESULT.SYSTEM_ERR.code,
                resultMsg : 'system error.'
            };
        }
        else
        {
            result = {
                resultCode : this.RESULT.SYSTEM_ERR.code,
                resultMsg : 'sql exception.'
            };
        }
        let body = {
            result: result
        };
        return res.send(body);
    }
    catch(err)
    {
        console.error(err);
        return new Error("Can't make response data...");
    }
};

sendResponseFailExceptionController = function(err, res){
    try {
        let result;
        result = {
            resultCode : this.RESULT.SYSTEM_ERR.code,
            resultMsg : 'api controller error.'
        };
        let body = {
            result: result,
            detail: err
        };
        return res.send(body);
    }
    catch(err) {
        console.error(err);
        return new Error("Can't make response data...");
    }
};

sendResponseFailExceptionService = function(err, res){
    try {
        let result;
        result = {
            resultCode : this.RESULT.SYSTEM_ERR.code,
            resultMsg : 'api service error.'
        };
        let body = {
            result: result
        };
        return res.send(body);
    }
    catch(err) {
        console.error(err);
        return new Error("Can't make response data...");
    }
};

sendResponseFailAuth = function(req, res) {
    try {
        let result;
        result = {
            resultCode : this.RESULT.UNAUTHORIZED.code,
            resultMsg : 'unauthorized.'
        };
        let body = {
            result: result
        };
        return res.send(body);
    }
    catch(err) {
        console.error(err);
        return new Error("Can't make response data...");
    }
};

module.exports = {
    RESULT,
    RESULT_ENUM,
    METHOD_TYPE,
    RESULT_VALID_NID,
    sendResultToClient,
    setResultToController,
    checkErrorCode,
    getErrorString,
    checkValidationToken,
    sendResponseResult,
    sendResponseFailDataNid,
    sendResponseFailDataNotNId,
    sendResponseFailDataParameter,
    sendResponseFailData,
    sendResponseFailSystem,
    sendResponseFailExceptionController,
    sendResponseFailExceptionService,
    sendResponseFailAuth
};
