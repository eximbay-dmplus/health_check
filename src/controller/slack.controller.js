const response = require('../common/response');
const Slack = require('slack-node');  // 슬랙 모듈 사용

async function healthcheck(req, res) {
    try {
        //https://hooks.slack.com/services/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
        // apiToken = "xoxb-321660083479-1371674322774-Jq2g8NKWZ2zv8KAmITd3IUNo";
        // const slack = new Slack(apiToken);
        //
        // const send = async(message) => {
        //     slack.api('chat.postMessage', {
        //         username: 'health-bot',  // 슬랙에 표시될 봇이름
        //         text:message,
        //         channel:'#health_check'  // 전송될 채널 및 유저
        //     }, function(err, response){
        //         console.log(response);
        //         response.sendResultToClient(response.RESULT.SUCCESS.code, msg, response);
        //     });
        // }
        //

        let msg = req.query.msg;

        // const webhookUri = "https://hooks.slack.com/services/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";  // Webhook URL
        // const webhookUri = "https://hooks.slack.com/services/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";  // Chainrefund Webhook URL
        const webhookUri = "https://hooks.slack.com/services/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";  // Detion Webhook URL

        const slack = new Slack();
        slack.setWebhook(webhookUri);

        const send = async(message) => {
            slack.webhook({
                text : message,
                // attachments:[
                //     {
                //         fallback:"링크주소: <https://www.google.com|구글>",
                //         pretext:"링크주소: <https://www.google.com|구글>",
                //         color:"#00FFFF",
                //         fields:[
                //             {
                //                 title:"알림",
                //                 value:"해당링크를 클릭하여 검색해 보세요.",
                //                 short:false
                //             }
                //         ]
                //     }
                // ]
            }, function(err, response){
                console.log(response);
            });
        }

        send(msg);

        response.sendResultToClient(response.RESULT.SUCCESS.code, msg, res);

    } catch (error) {
        console.log(`FAIL TO SEND MESSAGE`);
        if (error.cd) {
            response.sendResultToClient(error.cd, error.data, res);
        } else {
            response.sendResultToClient(response.RESULT.SYSTEM_ERR.code, error, res);
        }
    }

}

module.exports = {
    healthcheck
}