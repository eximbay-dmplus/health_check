const express = require('express');
const router = express.Router();
const slackController = require('../controller/slack.controller');

//if(process.env.PORT == 5531){
router.get('/v1.0/slack/healthcheck', slackController.healthcheck);
//}
module.exports = router;