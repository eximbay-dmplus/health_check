const createError = require('http-errors');
const express = require('express');
const path = require('path');
// const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const app = express();

/**
 * set global config for this server
 * @ dev: pc(postman -> pc(taxrefund.ccs.api.blockchain) -> vm(hyperledger network))
 * @ pc_to_test: pc(postman) -> pc(taxrefund.ccs.api.blockchain) -> hyperledger network
 * @ test: pc(postman) -> aws test(taxrefund.ccs.api.blockchain) -> hyperledger network
 * @ prod: pc(postman) -> aws prod(taxrefund.ccs.api.blockchain) -> hyperledger network
 */


app.use(cors());

logger.token('dev', (req,res) => {
  return `${res.get('code')} ${req.method} ${req.protocol} ${req.ip} ${req.path}`;
});
app.use(logger(':dev - :response-time ms'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


const slackRouter = require('./src/router/slack.router');


app.use('/api/slack', slackRouter);

module.exports = app;
